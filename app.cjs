// Ensure that error handling is well tested.
// If there is an error at any point, the subsequent solutions must not get executed.
// Solutions without error handling will get rejected and you will be marked as having not completed this drill.
// Usage of async and await is not allowed. Solve this using Promises only.

// Users API url: https://jsonplaceholder.typicode.com/users
// Todos API url: https://jsonplaceholder.typicode.com/todos

// Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
// Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

// On browser I should be able to see the todos with the user's name prefixed with @. For example it looks like this in HTML using Checkbox:
//  [  ] @Mayank Bring Mercedes Benz
//  [  ] @siva Buy a BMW X7
//  [  ] @Venkateshwar Buy Bugatti Chiron Sport 

// Don't implement entire To Do functionality.


fetch('https://jsonplaceholder.typicode.com/users').then((response)=> response.json())
.then((userData)=>{
    let userArrayPromise=userData.map((item)=>{
        return fetch(`https://jsonplaceholder.typicode.com/todos?userId=${item.id}`);
    })
    return Promise.all([userData,Promise.all(userArrayPromise)])
})
.then(([userData,todosList])=>{
    let userArrayPending=todosList.map((item)=>{
        return item.json();
    })
    return Promise.all([userData, Promise.all(userArrayPending)]);
})
.then(([userData,todosSeven])=>{
    todosSeven.forEach((item,index)=>{
        renderDom({
            userName:userData[index].name,
            todos:item,
        });
    })
})
.catch((error)=> console.log(error))
